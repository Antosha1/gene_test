def remove_empty_string(data_path, clean_data_path):
    with open(data_path, 'r') as f:
        data = f.read().splitlines()

    indexes_to_remove = []

    for i in range(len(data) - 1):
        if (data[i] == '') and (data[i + 1] == ''):
            indexes_to_remove.append(i)

    data = [elem for idx, elem in enumerate(data) if idx not in indexes_to_remove]

    with open(clean_data_path, 'w') as f:
        print(*data, file=f, sep='\n')


def _subarray_search(array, subarray):
    """
    :param array: input array
    :param subarray: subarray to search in array
    :return: start and end indexes of subarray location in array
    """
    window = len(subarray)
    indexes = []
    for i in range(len(array) - window):
        current = array[i:i + window]
        if ' '.join(subarray) in ' '.join(current):
            indexes.append((i, i + len(subarray) - 1))
    return indexes


def to_conll(data_path, save_data_path):
    with open(data_path, 'r') as f:
        data = f.read().splitlines()

    texts = []
    labels = []
    example = []
    for string in data:
        if string:
            example.append(string)
        else:
            text = example[0].split('||')[-1]
            texts.append(text)

            genes = [elem.split()[:-1] for elem in example[1:]]
            window_size = len(sorted(genes, key=len)[-1])
            tag_sequence = ['O' for _ in range(len(text.split()))]
            for gene in genes:
                ids = _subarray_search(text.split(), gene)
                for start_id, end_id in ids:
                    tag_sequence[start_id] = 'B-GENE'
                    for i in range(start_id + 1, end_id + 1):
                        tag_sequence[i] = 'I-GENE'
            labels.append(' '.join(tag_sequence))
            example = []

    with open(save_data_path, 'w') as f:
        for i in range(len(texts)):
            text = texts[i]
            label = labels[i]
            for (x, y) in zip(text.split(), label.split()):
                f.write(x + ' ' + y + '\n')
            f.write('\n')


if __name__ == '__main__':
    remove_empty_string('../data/genes.txt', '../data/clean_genes.txt')
    to_conll('../data/clean_genes.txt', '../data/conll_genes.txt')
