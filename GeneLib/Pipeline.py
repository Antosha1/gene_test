import torch
import torchtext
import numpy as np
import matplotlib.pyplot as plt

from GeneLib.model import RNNTagger
from GeneLib.utils import read_data, evaluate_iob, prf

from collections import defaultdict, Counter
import time


class GenePipeline:

    def __init__(self, train_data_path, val_data_path, lower=False):
        self.TEXT = torchtext.legacy.data.Field(init_token='<sos>', eos_token='<eos>', sequential=True, lower=lower)
        self.LABEL = torchtext.legacy.data.Field(init_token='<sos>', eos_token='<eos>', sequential=True, unk_token=None)
        self.fields = [('text', self.TEXT), ('label', self.LABEL)]
        self.device = 'cpu'

        self.train_examples = read_data(train_data_path, self.fields)
        self.valid_examples = read_data(val_data_path, self.fields)

        self.TEXT.build_vocab(self.train_examples, max_size=5000)
        self.LABEL.build_vocab(self.train_examples)

        self.model = RNNTagger(self.TEXT, self.LABEL, emb_dim=300, rnn_size=128)

    def inference(self, sentences):
        examples = []
        for sen in sentences:
            labels = ['O'] * len(sen)
            examples.append(torchtext.legacy.data.Example.fromlist([sen, labels], self.fields))
        dataset = torchtext.legacy.data.Dataset(examples, self.fields)

        iterator = torchtext.legacy.data.Iterator(
            dataset,
            device=self.device,
            batch_size=1,
            repeat=False,
            train=False,
            sort=False)

        out = []
        self.model.eval()
        with torch.no_grad():
            for batch in iterator:
                predicted = self.model.predict(batch.text)

                for tokens, pred_sen in zip(sentences, predicted):
                    out.append([self.LABEL.vocab.itos[pred_id] for _, pred_id in zip(tokens, pred_sen[1:])])
        return out

    def train(self, n_epochs, batch_size):
        n_tokens_train = 0
        n_sentences_train = 0
        for ex in self.train_examples:
            n_tokens_train += len(ex.text) + 2
            n_sentences_train += 1
        n_tokens_valid = 0

        for ex in self.valid_examples:
            n_tokens_valid += len(ex.text)

        self.model.to(self.device)
        optimizer = torch.optim.Adam(self.model.parameters(), lr=0.01, weight_decay=1e-5)

        n_batches = np.ceil(n_sentences_train / batch_size)
        mean_n_tokens = n_tokens_train / n_batches

        train_iterator = torchtext.legacy.data.BucketIterator(
            self.train_examples,
            device=self.device,
            batch_size=batch_size,
            sort_key=lambda x: len(x.text),
            repeat=False,
            train=True,
            sort=True)

        valid_iterator = torchtext.legacy.data.BucketIterator(
            self.valid_examples,
            device=self.device,
            batch_size=batch_size,
            sort_key=lambda x: len(x.text),
            repeat=False,
            train=False,
            sort=True)

        train_batches = list(train_iterator)
        valid_batches = list(valid_iterator)
        history = defaultdict(list)

        for i in range(1, n_epochs + 1):
            t0 = time.time()
            loss_sum = 0
            self.model.train()

            # Training
            for batch in train_batches:
                loss = self.model(batch.text, batch.label) / mean_n_tokens
                optimizer.zero_grad()
                loss.backward()
                optimizer.step()
                loss_sum += loss.item()

            train_loss = loss_sum / n_batches
            history['train_loss'].append(train_loss)

            # Validation
            stats = defaultdict(Counter)

            self.model.eval()
            with torch.no_grad():
                # Т.к. в этом задании обучение=валидация, то не будем записывать лосс на валидации
                for batch in valid_batches:
                    predicted = self.model.predict(batch.text)
                    evaluate_iob(predicted, batch.label, self.LABEL, stats)

            _, _, val_f1 = prf(stats['total'])

            history['val_f1'].append(val_f1)

            t1 = time.time()
            print(f'Epoch {i}: train loss = {train_loss:.4f}, val f1: {val_f1:.4f}, time = {t1 - t0:.4f}')

        print()
        print('Final evaluation on the validation set:')
        p, r, f1 = prf(stats['total'])
        print(f'Overall: P = {p:.4f}, R = {r:.4f}, F1 = {f1:.4f}')
        for label in stats:
            if label != 'total':
                p, r, f1 = prf(stats[label])
                print(f'{label:4s}: P = {p:.4f}, R = {r:.4f}, F1 = {f1:.4f}')

        fig = plt.figure(figsize=(15, 6))

        plt.subplot(1, 2, 1)
        plt.plot(history['train_loss'])
        plt.legend(['training loss'])

        plt.subplot(1, 2, 2)
        plt.plot(history['val_f1'], color='orange')
        plt.legend(['val F1-score'])

    def save(self, path_to_save):
        torch.save(self.model, path_to_save)

    def load(self, path_to_model):
        self.model = torch.load(path_to_model)
