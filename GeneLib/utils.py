import torchtext


def read_data(corpus_file, datafields):
    with open(corpus_file, encoding='utf-8') as f:
        examples = []
        texts = []
        labels = []
        for line in f:
            line = line.strip()
            if not line:
                examples.append(torchtext.legacy.data.Example.fromlist([texts, labels], datafields))
                texts = []
                labels = []
            else:
                columns = line.split()
                texts.append(columns[0])
                labels.append(columns[-1])
        return torchtext.legacy.data.Dataset(examples, datafields)


def to_spans(l_ids, voc):
    spans = {}
    current_label = None
    current_start = None
    for i, label_id in enumerate(l_ids):
        label = voc[label_id]

        if label[0] == 'B':
            if current_label:
                spans[current_start] = (current_label, i)
            current_label = label[2:]
            current_start = i
        elif label[0] == 'I':
            if current_label:
                if current_label != label[2:]:
                    spans[current_start] = (current_label, i)
                    current_label = label[2:]
                    current_start = i
            else:
                current_label = label[2:]
                current_start = i
        else:
            if current_label:
                spans[current_start] = (current_label, i)
                current_label = None
                current_start = None
    return spans


def compare(gold, pred, stats):
    for start, (label, end) in gold.items():
        stats['total']['gold'] += 1
        stats[label]['gold'] += 1
    for start, (label, end) in pred.items():
        stats['total']['pred'] += 1
        stats[label]['pred'] += 1
    for start, (gold_label, gold_end) in gold.items():
        if start in pred:
            pred_label, pred_end = pred[start]
            if gold_label == pred_label and gold_end == pred_end:
                stats['total']['corr'] += 1
                stats[gold_label]['corr'] += 1


def evaluate_iob(predicted, gold, label_field, stats):
    gold_cpu = gold.t().cpu().numpy()
    gold_cpu = list(gold_cpu.reshape(-1))

    pred_cpu = [l for sen in predicted for l in sen]

    gold_spans = to_spans(gold_cpu, label_field.vocab.itos)
    pred_spans = to_spans(pred_cpu, label_field.vocab.itos)

    compare(gold_spans, pred_spans, stats)


def prf(stats):
    if stats['pred'] == 0:
        return 0, 0, 0
    p = stats['corr']/stats['pred']
    r = stats['corr']/stats['gold']
    if p > 0 and r > 0:
        f = 2*p*r/(p+r)
    else:
        f = 0
    return p, r, f


def print_tags(model, sentence):
    tokens = sentence.split()
    tags = model.inference([tokens])[0]
    for token, tag in zip(tokens, tags):
        print(f'{token:15s}{tag}')
