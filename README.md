## Data
Genes corpus in ConLL format available [here](https://gitlab.com/Antosha1/gene_test/-/blob/master/data/conll_genes.txt)


## Prerequisites

To set up your development environment use:
```
pip install -r requirements.txt
```

## Commands

#### train.py

Script for training a model.

| Argument     | Required | Description                          |
|:-------------|:---------|--------------------------------------|
| -train       | true     | path to the train data               |
| -val         | true     | path to the validation data          |
| -epoch       | true     | number of epochs                     |
| -batch       | true     | path to the input file with data     |
| -o           | true     | path to save trained model           |


#### predict.py

Script for model evaluation (doesn't work yet. If you want to make predictions, do it with [notebook](https://gitlab.com/Antosha1/gene_test/-/blob/master/notebooks/Full_Pipeline.ipynb)).

| Argument | Required | Description                                      |
|:---------|:---------|--------------------------------------------------|
| -i       | true     | path to input file with data                     |
| -m       | true     | path to .pkl serialized model                    |
| -i       | true     | path to input file with data                     |
| -i       | true     | path to input file with data                     |


## Results

![alt text](figures/loss_f.png "Loss and F-score graphics")

| Model | Epochs | F-score                                      |
|:---------|:---------|--------------------------------------------------|
| BiLSTM       | 5     | 0.83                     |

