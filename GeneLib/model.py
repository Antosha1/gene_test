import torch
from torch import nn


class RNNTagger(nn.Module):

    def __init__(self, text_field, label_field, emb_dim, rnn_size):
        super().__init__()

        voc_size = len(text_field.vocab)
        self.n_labels = len(label_field.vocab)
        self.embedding = nn.Embedding(voc_size, emb_dim)
        self.rnn = nn.LSTM(input_size=emb_dim, hidden_size=rnn_size,
                           bidirectional=True, num_layers=1)
        self.top_layer = nn.Linear(2 * rnn_size, self.n_labels)

        self.pad_word_id = text_field.vocab.stoi[text_field.pad_token]
        self.pad_label_id = label_field.vocab.stoi[label_field.pad_token]
        self.loss = torch.nn.CrossEntropyLoss(reduction='sum')

    def compute_outputs(self, sentences):
        embedded = self.embedding(sentences)
        rnn_out, _ = self.rnn(embedded)
        out = self.top_layer(rnn_out)

        pad_mask = (sentences == self.pad_word_id).float()
        out[:, :, self.pad_label_id] += pad_mask * 10000
        return out

    def forward(self, sentences, labels):
        scores = self.compute_outputs(sentences)
        scores = scores.view(-1, self.n_labels)
        labels = labels.view(-1)
        return self.loss(scores, labels)

    def predict(self, sentences):
        scores = self.compute_outputs(sentences)
        predicted = scores.argmax(dim=2)
        return predicted.t().cpu().numpy()
