from GeneLib.Pipeline import GenePipeline
from GeneLib.utils import print_tags
import argparse


parser = argparse.ArgumentParser(description='Test model.')
parser.add_argument('-train', type=str, nargs='+',
                    help='path to the train data')
parser.add_argument('-val', type=str, nargs='+',
                    help='path to the validation data')
parser.add_argument('-m', type=str, nargs='+',
                    help='path to model')
parser.add_argument('-s', type=str, nargs='+',
                    help='inference example')

args = parser.parse_args()

model = GenePipeline(args.train[0], args.val[0]).load(args.m[0])
print('Load model successful')
print_tags(model, args.s[0])
