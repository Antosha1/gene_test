from GeneLib.Pipeline import GenePipeline
import argparse


parser = argparse.ArgumentParser(description='Test model.')
parser.add_argument('-train', type=str, nargs='+',
                    help='path to the train data')
parser.add_argument('-val', type=str, nargs='+',
                    help='path to the validation data')
parser.add_argument('-epoch', type=str, nargs='+',
                    help='number of epochs')
parser.add_argument('-batch', type=str, nargs='+',
                    help='path to the input file with data')
parser.add_argument('-o', type=str, nargs='+',
                    help='path to save trained model')

args = parser.parse_args()

print('Model training...')
model = GenePipeline(args.train[0], args.val[0])
model.train(int(args.epoch[0]), int(args.batch[0]))
print('Successful!')
print()
model.save(args.o[0])
print(f'Model saved in {args.o[0]}')
